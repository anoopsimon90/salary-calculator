# Salary Calculator!

This is my first ever react app . Simple and Filled with loads of defects :)

# How to run the app

Make sure you have installed **NodeJS !**
I have chosen **Visual Studio Code!** for developing this app
Use command **npm start** for starting development server and running the app(run this command from the project root folder)

# About the App
- Salary Calculator - Still work in progress , developed for self learning purpose
- Component based structure , All components are available in **/src/components** path . I have used Javascript -  XML(.jsx) for writing components
- Used **Bootstrap** for styling(css)

# Workflow
`index.js` file calls `<App>` component , app calls `<Calculator>` component, that invokes child component `<Results>` and the results are rendered in browser :) 
