import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Calculator from './components/calculator';
import Result from './components/result';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
