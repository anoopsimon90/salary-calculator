import React, { Component } from "react";
import Result from "./result";
class Calculator extends Component {
  state = {
    weeklyPay: 1041.67,
    fortnightlyPay: 2083.33,
    monthlyPay: 4166.67,
    annualPay: 50000,
    year: "2018-2019",
    salaryType: ["Annually", "Monthly", "Fortnightly"],
    taxYear: [2017, 2018, 2019],
    SuperannuationDefault: 9.5,
    salaryDefault: 50000,
    imgUrl:
      "https://www.publicdomainpictures.net/pictures/200000/nahled/plain-black-background.jpg",
    intro:
      "Use this simple, accurate tax calculator to work out how much you will be paid.  \
       This calculator now includes tax rates for the 2018-2019 tax year as well as the legislated changes in 2022 and 2024. \
       The most significant change is the introduction of a new low and middile income tax offset. \
       This offset will be offered as a tax return at the end of the 2018-2019 tax year. \
       This new offset is in addition to the existing low income offset. The 32.5% tax bracket has also been extended from $87,000 to $90,000. \
       Read more about the 2018 budget"
  };

  intro = {
    fontSize: 11,
    color: "grey",
    margin: 10
  };
  container = {
    width: 700,
    //margin: "0 auto",
    border: 20,
    height: "auto"
  };

  handleChange(event) {
    //Total Pay
    console.log(event.target.value);
    this.state.weeklyPay = (event.target.value / 48).toFixed(2);
    this.state.fortnightlyPay = (event.target.value / 24).toFixed(2);
    this.state.monthlyPay = (event.target.value / 12).toFixed(2);
    this.state.annualPay = (event.target.value * 1).toFixed(2);

    this.setState({ title: event.target.value });
  }

  renderSalaryType = () =>
    this.state.salaryType.map(salary => <option key={salary}>{salary}</option>);

  renderTaxYear = () =>
    this.state.taxYear.map(year => <option key={year}>{year}</option>);

  renderCalculatedOutput() {
    this.state.annualPay = this.state.salaryDefault * 9;
  }

  render() {
    // This component renders the input field
    return (
      <div className="container border" style={this.container}>
        <p style={this.intro}>{this.state.intro}</p>
        <div className="row" style={{ margin: 20 }}>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">Your Salary $</span>
            </div>
            <input
              type="text"
              className="form-control"
              aria-label="Amount (to the nearest dollar)"
              placeholder={this.state.salaryDefault}
              onChange={this.handleChange.bind(this)}
              id="salary"
              //value= {this.state.salaryDefault}
            />

            <div className="input-group-append">
              <span className="input-group-text">.00</span>
            </div>
            <select className="form-control">{this.renderSalaryType()}</select>
          </div>
          <div className="input-group-prepend">
            <span className="input-group-text">Tax Year</span>
            <select className="form-control">{this.renderTaxYear()}</select>
          </div>
          <div className="input-group-prepend" style={{ marginLeft: "auto" }}>
            <span className="input-group-text">Superannuation</span>
            <input
              type="text"
              className="form-control"
              aria-label="Amount (to the nearest dollar)"
              placeholder={this.state.SuperannuationDefault}
              id="demo"
            />
            <div className="input-group-append">
              <span className="input-group-text">%</span>
            </div>
          </div>
        </div>
        {/* Render Result , this component accepts state from parent component */}
        <Result state={this.state} />
      </div>
    );
  }
}

export default Calculator;
