import React, { Component } from 'react';   
import CopyToClipboard from 'react-copy-to-clipboard';

class Result extends Component {
  
    calulatePercentage= (value , percentage) =>  (value * (percentage/100) ).toFixed(2);
    state={}
   
    render() { 
        return (  <div style={{ margin: 50 }}>
            <table
              className="table table-hover table-dark"
              style={{ marginTop: 20, marginLeft: 40, width: "auto" }}
            >
              <thead>
                <tr>
                  <th scope="col">{this.props.state.year}</th>
                  <th scope="col">Weekly</th>
                  <th scope="col">Fortnightly</th>
                  <th scope="col">Monthly</th>
                  <th scope="col">Annually</th>
                </tr>
                <tr>
                  <td>Pay</td>
                  <td>{(this.props.state.weeklyPay - this.calulatePercentage(this.props.state.weeklyPay,32)).toFixed(2)}</td>
                  <td id="weeklyPay">{(this.props.state.fortnightlyPay - this.calulatePercentage(this.props.state.fortnightlyPay,32)).toFixed(2)}</td>
                  <td>{(this.props.state.monthlyPay - this.calulatePercentage(this.props.state.monthlyPay,32)).toFixed(2)}</td>
                  <td>{(this.props.state.annualPay - this.calulatePercentage(this.props.state.annualPay,32)).toFixed(2)}</td>
                </tr>
                <tr>
                  <td>Taxable Income</td>
                  <td>{this.props.state.weeklyPay}</td>
                  <td>{this.props.state.fortnightlyPay}</td>
                  <td>{this.props.state.monthlyPay}</td>
                  <td>{this.props.state.annualPay}</td>
                </tr>
              </thead>
            </table>
        

          </div>);
    }
}
 
export default Result;