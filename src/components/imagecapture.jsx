import React, { Component } from "react";

class ImageCapture extends Component {
  state = {
    items: [
      {
        format: "",
        width: "",
        height: "",
        filename: "",
        id: 0,
        author: "",
        author_url: "",
        post_url: ""
      }
    ]
  };
  render() {

    return (
      <ul>
        <li> {this.componentDidMount().items[0].author} </li>

      </ul>
    );
  }

  componentDidMount() {
    return fetch("https://picsum.photos/list")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            items: result[0]
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            error
          });
        }
      );
  }
}

export default ImageCapture;
