import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Calculator from './components/calculator';
import Result from './components/result';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      logo:"http://icons-for-free.com/free-icons/png/512/669953.png",

    }
    
  }
  state={
    logo :'http://icons-for-free.com/free-icons/png/512/669953.png',
  }

  onUpdate = (val) => {
    this.setState({
      logo: val
    })
  };
 
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={this.state.logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Salary Calculator</h1>
        </header>
        <p className="App-intro">
        
        </p>
        <Calculator />

        <div class="card-footer text-muted">
    Anoop Simon
  </div>
      </div>
    );
  }
}

export default App;
